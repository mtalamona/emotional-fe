const emotionsJson = {
  emotions: [
    { name: 'Muy Bien', id: 'grin-stars', color: '#009688' },
    { name: 'Bien', id: 'smile', color: '#F9FBE7' },
    { name: 'Neutro', id: 'meh', color: '#2196F3' },
    { name: 'Mal  ', id: 'frown', color: '#FF9800' },
    { name: 'Muy mal', id: 'sad-cry', color: '#E91E63' },
  ],
};
export default emotionsJson;
