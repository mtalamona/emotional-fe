const productsJson = {
  products: [
    { name: 'API Persona', id: '0001' },
    { name: 'API Poderes', id: '0002' },
    { name: 'API Referidos', id: '0003' },
    { name: 'API Transacciones', id: '0004' },
    { name: 'Calificación Sustentable', id: '0005' },
    { name: 'CRM', id: '0006' },
    { name: 'Delivery', id: '0007' },
    { name: 'eSign', id: '0008' },
    { name: 'EyP', id: '0009' },
    { name: 'OBE Canal', id: '0010' },
    { name: 'OBE Core', id: '0011' },
    { name: 'OB FFVV', id: '0012' },
    { name: 'OB Individuos', id: '0013' },
    { name: 'PIC', id: '0014' },
  ],

};
export default productsJson;
