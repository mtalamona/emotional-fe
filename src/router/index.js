import Vue from 'vue';
import Router from 'vue-router';
import Home from '../views/Home.vue';
import Afiliarse from '../views/Afiliarse.vue';
import Emocionar from '../views/Emocionar.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/emocionar',
      name: 'emocionar',
      component: Emocionar,
    },
    {
      path: '/afiliarse',
      name: 'afiliarse',
      component: Afiliarse,
    },
  ],
});
