import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);


export default new Vuex.Store({
  state: {
    emotion: [
      { id: '', name: '', color: '#000000' },
    ],
    application: [
      { id: '', name: '' },
    ],
    code: '',
  },
  mutations: {
    setEmotion(state, obj) {
      state.emotion.id = obj.id;
      state.emotion.name = obj.name;
      state.emotion.color = obj.color;
    },
    setApplication(state, obj) {
      state.application.id = obj.id;
      state.application.name = obj.name;
    },
    setCode(state, obj) {
      state.code = obj;
    },
  },
  getters: {
    getApplication(state) {
      return state.application;
    },
    getEmotion(state) {
      return state.emotion;
    },
  },
  actions: {
    setEmotion() {
      axios.post('/api/v1/emotions', {
        application_id: this.state.application.id,
        application: this.state.application.name,
        emotion_id: '1',
        emotion: this.state.emotion.name,
        sender: 'Tala',
        device_id: '1x1x1xx1',
        time_stamp: Vue.moment().format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]'),
      });
    },
    postEmotion(xcode, xemotionid) {
      axios.post('/api/v1/emote', {
        code: xcode,
        emotionid: xemotionid,
      });
    },
    validateCode(context, xcode) {
      console.log('validateCode() action - code: ');
      console.log(xcode);
      return axios.get('/api/v1/code', {
        params: {
          id: xcode,
        },
      })
        .then(() => this.commit('setCode', xcode));
    },
  },
});
