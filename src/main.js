import Vue from 'vue';
// import VueMoment from 'vue-moment';
import Button from 'primevue/button';
import InputText from 'primevue/inputtext';
import ListBox from 'primevue/listbox';
import SelectButton from 'primevue/selectbutton';
import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import router from './router/index';
import store from './store/index';
import App from './App.vue';


Vue.use(require('vue-moment'));

library.add(far);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('Button', Button);
Vue.component('InputText', InputText);
Vue.component('ListBox', ListBox);
Vue.component('SelectButton', SelectButton);

// eslint-disable-next-line import/first
import 'primevue/resources/themes/nova-light/theme.css';
// eslint-disable-next-line import/first
import 'primevue/resources/primevue.min.css';
// eslint-disable-next-line import/first
import 'primeicons/primeicons.css';


Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
